import { Cliente } from './cliente'
export const CLIENTES: Cliente[]=[
    { id:1, nombre:'Andres', apellido:'Guzman', email:'pr@ideas.com', createAt:'2017-12-11' },
    { id:2, nombre:'David', apellido:'Lopez', email:'ddv@outlook.com', createAt:'2017-12-12' },
    { id:3, nombre:'Alejandro', apellido:'Torres', email:'At@gmail.com', createAt:'2017-12-13' },
    { id:4, nombre:'Jhon', apellido:'Villegas', email:'JV@hotmail.com', createAt:'2017-12-14' },
    { id:5, nombre:'Javi', apellido:'Serbino', email:'jsr@yopmail.com', createAt:'2017-12-15' },
    { id:6, nombre:'Pablo', apellido:'Acosta', email:'pac@yahoo.com', createAt:'2017-12-16' },
    { id:7, nombre:'Manuel', apellido:'Perez', email:'manp@helpdesk-sv.com', createAt:'2017-12-17' },
    { id:8, nombre:'Gabriel', apellido:'Portillo', email:'gportillo@corinca.com', createAt:'2017-12-18' },
    { id:9, nombre:'Marlon', apellido:'Valle', email:'vallmar@calpi.com', createAt:'2017-12-19' },
    { id:10, nombre:'William', apellido:'Martinez', email:'crwmartinez@adoc.com', createAt:'2017-12-20' }
  ];