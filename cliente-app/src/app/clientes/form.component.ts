import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import {Router, ActivatedRoute} from '@angular/router'
import {ClienteService} from './cliente.service'
import swal from 'sweetalert2'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {
  cliente: Cliente=new Cliente();
  titulo: string="Crear Cliente";

  errores: string[];

  constructor(private clienteService: ClienteService, private router: Router, private activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarCliente();
  }

  cargarCliente():void{
    this.activatedRoute.params.subscribe(params=>{
      let id = params['id']
      if(id){
        this.clienteService.getCliente(id).subscribe((cliente)=>this.cliente=cliente)
      }
    } 

    )
  }
  public create(): void {
    this.clienteService.create(this.cliente)
    .subscribe( respuesta =>{
        this.router.navigate(['/clientes'])
        Swal.fire('Nuevo cliente', `${respuesta.mensaje}: ${respuesta.cliente.nombre}`,'success')
      },
      err=>{
        this.errores=err.error.errors as string[];
      } 
      );
  }
  update():void{
    this.clienteService.update(this.cliente)
    .subscribe(respuesta=>{
      this.router.navigate(['/clientes'])
      Swal.fire('Cliente Actualizado', `${respuesta.mensaje}: ${respuesta.cliente.nombre}`,'success')
    },
    err=>{
      this.errores=err.error.errors as string[];
    }
    )
  }

}
