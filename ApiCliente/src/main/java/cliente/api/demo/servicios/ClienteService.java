/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.api.demo.servicios;

import cliente.api.demo.modelos.Cliente;
import java.util.List;

/**
 *
 * @author kirio
 */
public interface ClienteService {

    public List<Cliente> findAll();

    public Cliente save(Cliente cliente);

    public Cliente findById(Long id);

    public void delete(Cliente cliente);

}
