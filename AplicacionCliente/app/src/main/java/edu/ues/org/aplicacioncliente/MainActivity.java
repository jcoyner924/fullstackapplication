package edu.ues.org.aplicacioncliente;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.ues.org.aplicacioncliente.model.Cliente;
import edu.ues.org.aplicacioncliente.utils.Apis;
import edu.ues.org.aplicacioncliente.utils.ClienteService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    List<Cliente> clientes=new ArrayList<>();
    private TextView mJsonTxtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mJsonTxtView=findViewById(R.id.jsonText);
        listarClientes();
    }

    public void listarClientes(){
        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl("http://192.168.0.21:8080/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ClienteService clienteService1=retrofit.create(ClienteService.class);

        Call<List<Cliente>> call=clienteService1.getClientes();
        call.enqueue(new Callback<List<Cliente>>() {
            @Override
            public void onResponse(Call<List<Cliente>> call, Response<List<Cliente>> response) {
                clientes=response.body();
                for (Cliente cliente:clientes){
                    String content="";
                    content+="ID"+cliente.getId()+"\n";
                    content+="NOMBRE"+cliente.getNombre()+"\n";
                    content+="APELLIDO"+cliente.getApellido()+"\n";
                    content+="CORREO"+cliente.getEmail()+"\n";
                    content+="FECHA"+cliente.getCreateAt()+"\n\n";
                    mJsonTxtView.append(content);
                }
            }

            @Override
            public void onFailure(Call<List<Cliente>> call,  Throwable t) {
                mJsonTxtView.setText(t.getMessage());
            }
        });
    }
}
