package edu.ues.org.aplicacioncliente.utils;

import java.util.List;

import edu.ues.org.aplicacioncliente.model.Cliente;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ClienteService {
    @GET("clientesT")
    Call<List<Cliente>> getClientes();
}
