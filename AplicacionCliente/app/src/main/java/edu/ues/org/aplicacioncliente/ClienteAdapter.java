package edu.ues.org.aplicacioncliente;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ues.org.aplicacioncliente.model.Cliente;

public class ClienteAdapter extends ArrayAdapter<Cliente> {
    private Context context;
    private List<Cliente> clientes;

    public ClienteAdapter(@NonNull Context context, int resource, @NonNull List<Cliente> objects) {
        super(context, resource, objects);
        this.context=context;
        this.clientes=objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView=layoutInflater.inflate(R.layout.content_main,parent,false);
        TextView txtIdCliente=(TextView)rowView.findViewById(R.id.ID);
        TextView txtNombre=(TextView)rowView.findViewById(R.id.NOMBRE);;
        TextView txtApellido=(TextView)rowView.findViewById(R.id.APELLIDO);;
        TextView txtEmail=(TextView)rowView.findViewById(R.id.EMAIL);;
        TextView txtCreateAt=(TextView)rowView.findViewById(R.id.CREATEAT);;

        txtIdCliente.setText(String.format("ID:%S",clientes.get(position).getId()));
        txtNombre.setText(String.format("NOMBRE:%S",clientes.get(position).getNombre()));
        txtApellido.setText(String.format("APELLIDO:%S",clientes.get(position).getApellido()));
        txtEmail.setText(String.format("EMAIL:%S",clientes.get(position).getEmail()));
        txtCreateAt.setText(String.format("FECHA:%S",clientes.get(position).getCreateAt()));

        return rowView;

    }

}
